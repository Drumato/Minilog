# 概要

簡単にロギングツール作っとけば便利かなぁと思い作った｡  

Pythonのモジュール検索パスにディレクトリ置いて

```python
from minilog import Minilog

minilogger = Minilog(10,'sample.log')
minilogger.logging('debug','needed refactoring')
```

のようにすれば使える｡
