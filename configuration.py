from logging import DEBUG,INFO,WARNING

def get_config(file_level=DEBUG,output_level=WARNING,filename=f'{__name__}.log'):
    CONFIGTABLE = {
            'version':1,
            #configuration format
            'formatters': {
                'customFormat': {
                    'format':"Line %(lineno)d :%(asctime)s %(levelname)s %(message)s",
                    'datefmt':"%Z %m/%d/%Y",
                    'style':'%'
                    },
                },
            #configuration handler
            'filters': {},
            'handlers': {
                'customStreamHandler': {
                    'class':'logging.StreamHandler',
                    'formatter':'customFormat',
                    'level':file_level
                    },
                'fileHandler': {
                    'level': INFO,
                    'formatter':'customFormat',
                    'class':'logging.handlers.RotatingFileHandler',
                    'filename':filename,
                    'maxBytes':1000000,
                    'backupCount':3,
                    'encoding':'utf-8'
                    }
                },
            #configuration target of logger
            'loggers': {
                'outputLogging': {
                    'handlers': ['customStreamHandler'],
                    'level': output_level,
                    'propagate': 0
                    },
                'fileLogging': {
                    'handlers': ['fileHandler'],
                    'level':file_level,
                    'proparagate':0
                    }
                }
            }
    return CONFIGTABLE
