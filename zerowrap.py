import logzero
LOGFUNC = ['debug','info','warning','error']
class Zerowrap:
    def __init__(self,level,filename=None):
        logzero.loglevel(level)
        if filename:
            logzero.logfile(filename,loglevel=30,maxBytes=4096,backupCount=3)


    def logging(self,logfunc,message):
        if logfunc in LOGFUNC:
            exec(f'logzero.logger.{logfunc}(message)')