from logging import getLogger
from logging.config import dictConfig

def setup(configfunc):
    dictConfig(configfunc())

def get_logger():
    return getLogger(__name__)
